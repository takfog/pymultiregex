import multiregex.rename as mrr

v = mrr.MultiRegexRename()
v.add(r'^.*?([1-9][0-9]*)[xe](\d+\D)', r'\1x\2')
v.add(r'\.', ' ')
v.add(r' (ita?|eng?)(\.srt)', r'.\1\2', part=mrr.fullname)
v.add(r' (?:ita|web(?:-dl)?mux)(?: [^.]*|)($|\.)', r'\1')


if __name__ == '__main__':
    import sys
    r = v.get_renames(sys.argv[1:], True)
    for x in r.items():
        print(x)

    v.rename(sys.argv[1:], flatten=True)
