import os


def create(dir, file):
    dir = os.path.join('test', 'files', dir)
    os.makedirs(dir, exist_ok=True)
    path = os.path.join(dir, file)
    if not os.path.exists(path):
        with open(path, 'w') as f:
            f.write(path)


def twd(num):
    return '7H3.W4LK1NG.D34D.10x' + str(num) + '.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine'


create('One Piece', 'One.Piece.01x01.Il.ragazzo.di.gomma.ita.eng.sub.eng.by.pir8.avi')
create('One Piece', 'One.Piece.01x01.Il.ragazzo.di.gomma.ita.eng.sub.eng.by.pir8.ita.srt')
create('One Piece', 'One.Piece.S1E02.Il.cacciatore.di.pirati.mkv')

folder = 'twd'
create(folder, 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(folder, 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(folder, 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(folder, 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(folder, 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(folder, 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(folder, 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(folder, 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(folder, 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(folder, 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(folder, 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(folder, 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(folder, 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(folder, 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')

create(twd(3), 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(3), 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(twd(3), 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(3), 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(3), 'The.Walking.Dead.10x03.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(twd(4), 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(4), 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(twd(4), 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(4), 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(4), 'The.Walking.Dead.10x04.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(twd(5), 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(5), 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(5), 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(5), 'The.Walking.Dead.10x05.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(twd(6), 'The.Walking.Dead.10x06.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(6), 'The.Walking.Dead.10x06.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(twd(6), 'The.Walking.Dead.10x06.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(6), 'The.Walking.Dead.10x06.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(7), 'The.Walking.Dead.10x07.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(7), 'The.Walking.Dead.10x07.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(twd(7), 'The.Walking.Dead.10x07.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(7), 'The.Walking.Dead.10x07.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(7), 'The.Walking.Dead.10x07.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')
create(twd(8), 'The.Walking.Dead.10x08.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.En.srt')
create(twd(8), 'The.Walking.Dead.10x08.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.Eng.srt')
create(twd(8), 'The.Walking.Dead.10x08.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.It.srt')
create(twd(8), 'The.Walking.Dead.10x08.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.avi')
create(twd(8), 'The.Walking.Dead.10x08.WEB-DLMux.XviD.Ita.Eng.Ac3.Earine.srt')