from ._multiregex import MultiRegexBase
import os
import shutil
from typing import List, Optional, Dict

name = "name"
fullname = "fullname"
ext = "ext"
path = "path"
full = "full"


class FileParts:
    def __init__(self, filename: str):
        self.path: Optional[str] = None
        self.name: Optional[str] = None
        self.ext: Optional[str] = None
        self.__split(filename)

    def __split(self, filename: str):
        self.path = os.path.dirname(filename)
        if self.path == '':
            self.path = None
        else:
            filename = os.path.basename(filename)

        self.__split_name(filename)

    def __split_name(self, filename: str):
        try:
            name_end = filename.rindex('.')
            if name_end == 0 or name_end == len(filename) - 1:
                name_end = None
        except ValueError:
            name_end = None

        if name_end is None:
            self.ext = None
            self.name = filename
        else:
            self.ext = filename[name_end + 1:]
            self.name = filename[:name_end]

    def get(self, part=full) -> Optional[str]:
        if part == name:
            return self.name
        if part == path:
            return self.path
        if part == ext:
            return self.ext

        fn = self.name + '.' + self.ext if self.ext else self.name

        if part == fullname:
            return fn
        if part == full:
            return os.path.join(self.path, fn) if self.path else fn

        return None

    def set(self, value: str, part):
        if part == name:
            self.name = value
        elif part == path:
            self.path = value
        elif part == ext:
            self.ext = value
        elif part == full:
            self.__split(value)
        elif part == fullname:
            self.__split_name(value)


class MultiRegexRename(MultiRegexBase):

    def __init__(self, defpart=name):
        super(MultiRegexRename, self).__init__()
        self.__defpart = defpart

    def _in2str(self, file: FileParts, info: dict) -> str:
        part = info.get('part', self.__defpart)
        return file.get(part)

    def _str2out(self, file: FileParts, output: str, info: dict) -> FileParts:
        file.set(output, info.get('part', self.__defpart))
        return file

    def _flatten(self, files: List[str]) -> List[str]:
        out = []
        for fn in files:
            if os.path.isfile(fn):
                out.append(fn)
            elif os.path.isdir(fn):
                for root, _, files in os.walk(fn):
                    out += [os.path.join(root, f) for f in files]
        return out

    def get_renames(self, files: List[str], flatten=False) -> Dict[str, str]:
        if flatten:
            files = self._flatten(files)
        renames = {}
        for fn in files:
            if fn in renames:
                continue
            file = FileParts(fn)
            out = self._run(file)
            renames[fn] = out.get()
        return renames

    def rename(self, files: List[str], flatten=False):
        renames = self.get_renames(files, flatten)
        for src, dest in renames.items():
            if src != dest:
                os.makedirs(os.path.dirname(dest), exist_ok=True)
                shutil.move(src, dest)
