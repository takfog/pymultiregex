import re
from typing import Union, List


class MultiRegexBase:
    def __init__(self):
        self._list = []
        self.ignorecase = True
        
    def add(self, regex, replace, **kwargs):
        ignorecase = kwargs.get('ignorecase', self.ignorecase)
        if ignorecase and 'flags' not in kwargs:
            kwargs['flags'] = re.IGNORECASE

        self._list.append((regex, replace, kwargs))
        return self

    def _in2str(self, input, info: dict) -> str:
        return input

    def _str2out(self, input, output: str, info: dict):
        return output

    def _run(self, input):
        data = input
        for pattern, replace, info in self._list:
            src = self._in2str(data, info)
            if src:
                dest = re.sub(pattern, replace, src, count=info.get('count', 0), flags=info['flags'])
                data = self._str2out(data, dest, info)
        return data


class MultiRegex(MultiRegexBase):

    def replace(self, input: Union[str, List[str]]) -> Union[str, List[str]]:
        if type(input) is str:
            return self._run(input)
        else:
            return [self.replace(v) for v in input]
